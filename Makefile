CC = g++ --std=c++14 -Wall -Wextra -Wpedantic
INCLUDE = -ID:\sdl2\mingw-2.0.8\x86_64-w64-mingw32\include\SDL2 \
-ID:\sdl2\SDL_Image\mingw-2.0.3\x86_64-w64-mingw32\include\SDL2 \
-ID:\GLEW\mingw-2.1.0\include\GL \
-ID:\GameDev\LearnOpenGL\include

LIB_DIR = -LD:\sdl2\mingw-2.0.8\x86_64-w64-mingw32\lib \
-LD:\sdl2\SDL_Image\mingw-2.0.3\x86_64-w64-mingw32\lib \
-LD:\GLEW\mingw-2.1.0\lib

LIBS = -w -Wl,-subsystem,windows -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lglew32 -lopengl32
SOURCES = src/main.cpp

all:
	$(CC) $(SOURCES) -o OpenGLTest.exe $(INCLUDE) $(LIB_DIR) $(LIBS)


clean:
	del OpenGLTest


run:
	OpenGLTest.exe
